var mongoose=require('mongoose');
const bicicleta = require('../../models/bicicleta');
var Bicicleta=require('../../models/bicicleta');

describe('Testing Bicicleta', function(){
    beforeAll( function (done) {
     
        var MongoDB='mongodb://localhost/testdb';
        mongoose.connect(MongoDB,{useNewUrlParser:true, useUnifiedTopology: true}) ;
        const db = mongoose.connection;
     
       db.on('error',console.error.bind(console,'connection error'));
       db.once('open',function(){
           console.log('conectado');
           done();
       });
    });
    afterAll(function (done) {
            
        Bicicleta.deleteMany({}, function(err,success){
            if(err)
                console.log(err);
             
                done();                            
        });        
    });
    
    describe('Bicicle.createInstance',()=>{
        it ('crear una instancia',()=>{
            var bici=Bicicleta.createInstance(1,"verde","Urbana",[-34.5,-54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
        });
    });

    
    describe('Bicicleta.allBicis',()=>{
        it ('comienza vacia',(done)=>{
            
           Bicicleta.allBicis(function(err,bicis){
                
               expect(bicis.length).toEqual(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',()=>{
        it ('agregar solo una bici',(done)=>{
            var aBici=new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    //expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    
                    done();
                });
            });
        });
    });


    describe('Bicicleta.findbycode',()=>{
        it('debe devolver',(done)=>{
            Bicicleta.allBicis(function (err,bicis) {
                expect(bicis.length).toBe(0);

                var aBici=new Bicicleta({code:1, color:"verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2=new Bicicleta({code:2, color:"negro", modelo:"ciudad"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1,function(error,targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            done();
                        });
                    });
                });
            });
        });
    });




});

/*
beforeEach(()=>{
    Bicicleta.allBicis=[];
   
});



describe ('Bicicleta.allBicis', ()=>{
    it('comienza vacia',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});


describe('Bicicleta.add',()=>{
    it('agregar una',()=>{
        
        expect(Bicicleta.allBicis.length).toBe(0);
        var f=  new Bicicleta(3,'aaaazul','rurbana',[-33.6112424,-57.3968287]);
        Bicicleta.add(f);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(f);
    });
});

describe('Bicicleta.findById',()=>{
    it('buscar una una',()=>{
      
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[-34.6012424,-58.3861497]);
        var b=  new Bicicleta(2,'azul','urbana',[-35.5912424,-59.3868287]);
        var s=  new Bicicleta(3,'aaaazul','rurbana',[-33.6112424,-57.3968287]);

        Bicicleta.add(a);
        Bicicleta.add(b);
        Bicicleta.add(s);
        
        expect(Bicicleta.allBicis.length).toBe(3);
       

        expect(Bicicleta.findById(2)).toBe(b);
        expect(Bicicleta.findById(1)).toBe(a);
        expect(Bicicleta.findById(3)).toBe(s);

    });
});*/


