var mongoose=require('mongoose');
var Reserva=require('./reserva');
const bcrypt=require('bcrypt');
const saltRounds=10;
const uniqueValidator=require('mongoose-unique-validator');

var Schema=mongoose.Schema;
const validateEmail=function(email){
    const re=/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    return re.test(email);
}

var usuarioSchema=new Schema({
    nombre:{
        tye:String,
        trim:true,
        required:[true, "El nombre es obligatorio"]
    },
    email:{
        type: String,
        trim:true,
        required:[true, "El mail es obligatorio"],
        lowercase :true,
        unique:true,
        validate:[validateEmail,'Por favor ingrese un Email valido'],
        match:[/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g]

    },
    password:{
        type:String,
        required:[true,'La contraseña es obligatorioa']

    },

    passwordResetToken:String,
    passwordResetTokenExpires:Date,
    verificado:{
        type:Boolean,
        default:false
    }

});
usuarioSchema.plugin(uniqueValidator, {message:'El correo ya existe'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password=bcrypt.hashSync(this.password,saltRounds)
    }
    next();
});

usuarioSchema.methods.validPassword =function(password){
    return bcrypt.compare(password, this.password);
}

usuarioSchema.methods.reservar=function(biciI, desde,hasta,cb){
    var reserva=new Reserva({usuario:this._id,bicicleta:biciId,desde:desde,hasta:hasta});
    console.log(reserva);
    reserva.save(cb);
}

module.exports=mongoose.model('Usuario',usuarioSchema);