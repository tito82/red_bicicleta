/*var map=L.map('main_map').setView([-34.6012424,-58.3861497],13);

L.tileLayer('https://{s}.title.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> Contributors'
}).addTo(map);
*/



// Initialize the map
// [50, -0.1] are the latitude and longitude
// 4 is the zoom
// mapid is the id of the div where the map will appear
var map = L
  .map('main_map')
  .setView([-34.6012424,-58.3861497],13);

// Tile type: openstreetmap normal
var openstreetmap = L.tileLayer(
  'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
  maxZoom: 12
})

// Tile type: openstreetmap Hot
var openstreetmapHot = L.tileLayer(
  'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
  maxZoom: 6
})

// Tile type: openstreetmap Osm
var openstreetmapOsm = L.tileLayer(
  'http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
  maxZoom: 3
})

//Base layers definition and addition
var allOptions = {
	"Open streetmap": openstreetmap,
  "Open streetmap: Hot": openstreetmapHot,
  "Open streetmap: Osm": openstreetmapOsm
};

// Initialize with openstreetmap
openstreetmap.addTo(map);

// Add baseLayers to map as control layers
L.control.layers(allOptions).addTo(map);



$.ajax({
  dataType:"json",
  url:"api/bicicletas",
  success:function(result){
    console.log(result);
    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion,{title:bici.id}).addTo(map)
    });
    
  }
})