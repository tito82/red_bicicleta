var express= require('express');
var router= express.Router();
var bicicletaController=require('../controllers/bicicleta');

router.get('/',bicicletaController.bicicleta_List);

router.get('/create',bicicletaController.bicicleta_create_get);
router.post('/create',bicicletaController.bicicleta_create_pos);

router.post('/:id/delete',bicicletaController.bicicleta_delete_post);

router.get('/:id/update',bicicletaController.bicicleta_update_get);
router.post('/:id/update',bicicletaController.bicicleta_update_pos);



module.exports=router;